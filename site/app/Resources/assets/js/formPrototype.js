function handleFormPrototypeAsListItem($ulClassName, $entityName, $addMessage, $deleteMessage) {
   handleFormPrototypeAsListItem($ulClassName, $entityName, $addMessage, $deleteMessage, "list-group-item"); 
}

  function handleFormPrototypeAsListItem($ulClassName, $entityName, $addMessage, $deleteMessage, $liClassName) {
        
        var $collectionHolder;
	
        var $selector = 'ul.' + $ulClassName; 
               
        // Get the ul that holds the collection of forms
        $collectionHolder = $($selector);
        
        // add a delete link to all of the existing tag form li elements
        $collectionHolder.find('li').each(function() {            
            addListItemFormDeleteLink($(this), $deleteMessage);
        });

        // add the "add a tag" anchor and li to the tags ul
        var addATag = '<a href="#" class="btn btn-primary btn-sm add_' + $entityName + '_link"><i class="fa fa-plus"></i>' + $addMessage + '</a>'; 
        var $addLink = $(addATag);
        var $liTag = '<li></li>';
        if ($liClassName !== ""){
            $liTag = '<li class="'+$liClassName+'"></li>';
        }
        var $newLinkLi = $($liTag).append($addLink);

        $collectionHolder.append($newLinkLi);
        
        //setAddLinkEnableState($entityName);   

        // count the current form inputs we have (e.g. 2), use that as the new
        // index when inserting a new item (e.g. 2)
        $collectionHolder.data('index', $collectionHolder.find(':input').length);

        $addLink.on('click', function(e) {
            // prevent the link from creating a "#" on the URL
            e.preventDefault();
            // add a new tag form (see next code block)
            addFormAsListItem($collectionHolder, $newLinkLi, $deleteMessage, $entityName, $liTag);           
        });                   
    }
   	
    function addFormAsListItem($collectionHolder, $newLinkLi, $deleteMessage, $entityName, $liTag) {
        // Get the data-prototype explained earlier
        var prototype = $collectionHolder.data('prototype');

        // get the new index
        var index = $collectionHolder.data('index');

        // Replace '__name__' in the prototype's HTML to
        // instead be a number based on how many items we have
        var newForm = prototype.replace(/__name__/g, index);

        // increase the index with one for the next item
        $collectionHolder.data('index', index + 1);

        // Display the form in the page in an li, before the "Add a contact" link li
        var $newFormLi = $($liTag).append(newForm);
        $newLinkLi.before($newFormLi);
        
        addListItemFormDeleteLink($newFormLi, $deleteMessage, $entityName);
    }
    
    function addListItemFormDeleteLink($formLi, $deleteMessage, $entityName) {
	
        var deleteATag = '<a class="btn btn-sm btn-primary delete_link" href="#"><i class="fa fa-trash"></i>'+ $deleteMessage + '</a>'; 
	
        var $removeFormA = $(deleteATag);
        $formLi.append($removeFormA);

        $removeFormA.on('click', function(e) {
            // prevent the link from creating a "#" on the URL
            e.preventDefault();

            //setAddLinkEnableState($entityName); 

            // remove the li for the tag form
            $formLi.remove();
            
            $("button[type=submit]").prop("disabled", false);                       
        });                  
    } 
    
    function setAddLinkEnableState($entityName) { 
                
        if ($collectionHolder.length < 3) {
            $("." + $entityName).prop("disabled", false);
        } else {
            $("." + $entityName).prop("disabled", true);
        }
    }
           
       
    function handleFormPrototypeAsTableRow($tableClassName, $entityName, $addMessage, $deleteMessage) {
        
        var $collectionHolder;
	
        var $selector = 'table.' + $tableClassName; 
               
        // Get the tr that holds the collection of forms
        $collectionHolder = $($selector);
        
        // add a delete link to all of the existing tag form tr elements
        $collectionHolder.find('tr.'+$tableClassName).each(function() {            
            addRowFormDeleteLink($(this), $deleteMessage);
        });

        // add the "add a tag" anchor and li to the tags ul
        var addATag = '<a href="#" class="btn btn-primary btn-sm add_' + $entityName + '_link"><i class="fa fa-plus"></i>' + $addMessage + '</a>'; 
        var $addLink = $('<td colspan="5"></td>').append(addATag);
        var $newLinkTr = $('<tr class="'+$tableClassName+'"></tr>').append($addLink);

        var $lastRow = $('table.'+$tableClassName+' tr:last'); 
        $lastRow.after($newLinkTr);
        
        // count the current form inputs we have (e.g. 2), use that as the new
        // index when inserting a new item (e.g. 2)
        $collectionHolder.data('index', $collectionHolder.find(':input').length);

        $addLink.on('click', function(e) {
            // prevent the link from creating a "#" on the URL
            e.preventDefault();
            // add a new tag form (see next code block)
            addFormAsTableRow($collectionHolder, $newLinkTr, $deleteMessage, $entityName, $tableClassName);
        });                   
    }     
    
    function addFormAsTableRow($collectionHolder, $newLinkTr, $deleteMessage, $entityName,$tableClassName) {
        // Get the data-prototype explained earlier
        var prototype = $collectionHolder.data('prototype');

        // get the new index
        var index = $collectionHolder.data('index');

        // Replace '__name__' in the prototype's HTML to
        // instead be a number based on how many items we have
        var newForm = prototype.replace(/__name__/g, index);

        // increase the index with one for the next item
        $collectionHolder.data('index', index + 1);

        // Display the form in the page in an li, before the "Add a contact" link li
        var $newFormTr = $('<tr class="'+$tableClassName+'"></tr>').append(newForm);
        $newLinkTr.before($newFormTr);
        
        addRowFormDeleteLink($newFormTr, $deleteMessage, $entityName);
    }
    
      function addRowFormDeleteLink($formTr, $deleteMessage, $entityName) {
	
        var deleteATag = '<a class="btn btn-sm btn-primary delete_link" href="#"><i class="fa fa-trash"></i>'+ $deleteMessage + '</a>'; 
	
        var $removeFormA = $('<td></td>').append(deleteATag);
        $formTr.append($removeFormA);

        $removeFormA.on('click', function(e) {
            // prevent the link from creating a "#" on the URL
            e.preventDefault();

            //setAddLinkEnableState($entityName); 
            // remove the li for the tag form
            $formTr.remove();
            
         //   $("button[type=submit]").prop("disabled", false);                       
        });                  
    }