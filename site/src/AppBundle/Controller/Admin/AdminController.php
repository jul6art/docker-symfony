<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\ORM\QueryBuilder;

/**
 * Admin controller.
 * 
 *
 */
abstract class AdminController extends \AppBundle\Controller\StandardController
{
    abstract protected function getFormFilterType(); //\AppBundle\FormFilter\SchoolFilterType::class
    abstract protected function getFormType();
    abstract protected function getType(); // \AppBundle\Entity\School::class
    abstract protected function getName(); // school
    
    protected function getNewTemplate(){
        return 'admin/default/new.html.twig';
    }
    protected function getEditTemplate(){
        return 'admin/default/edit.html.twig';
    }
    protected function getFormTemplate(){
        return 'default/form.html.twig';
    }
    
    protected function getTemplatePrefix(){
         return "app_admin_";
     }
     
     protected function getDefaultSortField(){
        return $this->getName().'.id';
    }
    protected function getDefaultSortOrder(){
        return 'desc';
    }
    /**
     * Lists all entities.
     *
     * @Route("/")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request) 
    {
        $repository = $this->getDoctrine()->getRepository($this->getType());        
        /* @var $query \Doctrine\ORM\QueryBuilder */
        $query = $repository->createQueryBuilder($this->getName());
        $form = $this->createForm($this->getFormFilterType());
        
        $itemsPerPage = \AppBundle\Constants\Constants::DEFAULT_PAGE_SIZE;
        $page = $request->query->get('page', 1);
        // default sort
        $sort = $this->getDefaultSortField();
        if ($request->query->has('sort')){
             $sort = $request->query->get('sort');
        }
        $this->addJoinBySortColumn($query, $sort);
        $direction = ($request->query->has('direction'))?$request->query->get('direction'):$this->getDefaultSortOrder();
        
        $form->handleRequest($request);
        if ($form->isSubmitted()&&!$form->isValid()) {
            $entities = NULL;
        } else {
            if ($form->isSubmitted()) {
                // save data in session
                $session = $this->get('session');
                $session->set($form->getName(), $request->request->get($form->getName()));
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $query);
               // $itemsPerPage = $form->get('itemsPerPage')->get('itemsPerPage')->getData();
                // Reset page number after search
                $page = 1;
            } else {
                // not submit get data in session
                $form = $this->createForm($this->getFormFilterType());
                $session = $request->getSession();
                if ($this->keepFilterForm($request,$form)){
                    $form->submit($session->get($form->getName()));
                 //   $itemsPerPage = $form->get('itemsPerPage')->get('itemsPerPage')->getData();
                    $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $query);
                }
            }
            $paginator = $this->get('knp_paginator');
            $entities = $paginator->paginate($query, $page/* page number */, $itemsPerPage, array('defaultSortFieldName' => $sort,
                'defaultSortDirection' => $direction, 'wrap-queries' => true)
            );
        }
        return $this->render('admin/'.$this->getName().'/index.html.twig', array(
            'entities' => $entities, 'form' => $form->createView()
        ));
    }
    

    /**
     * Creates a new entity.
     *
     * @Route("/new/")
     * @Method({"GET","POST"})
     */
    public function newAction(Request $request)
    {
        $entity = $this->createEntity();
        
        $form = $this->createForm($this->getFormType(),$entity);
        $form->handleRequest($request);
        
        if($request->isXmlHttpRequest()){
             
             if ($form->isSubmitted()) {
                if ($form->isValid()){
                    // Requete ajax
                    $this->saveEntity($entity);
                    $message = 'OK';
                    $response = new Response();
                    $response->headers->set('Content-Type', 'application/html');
                    $response->setContent($message);

                    $this->get('session')->getFlashBag()->add(
                        'success','entity.save.success'
                    );
                    return $response;
                } else {// NOT VALID
                    return $this->render($this->getFormTemplate(), array(
                        'edit_form' => $form->createView(),
                    ));
                }
             }
        } else {
            if ($form->isSubmitted() && $form->isValid()) {
                $this->saveEntity($entity);
                $this->get('session')->getFlashBag()->add(
                    'success','entity.save.success'
                );
                return $this->redirectToRoute($this->getTemplatePrefix().$this->getName().'_index');
            }
        }
        
        return $this->render($this->getNewTemplate(), array(
            'entity' => $entity,
            'form_template' => $this->getFormTemplate(),
            'edit_form' => $form->createView(),
            'new_route' => $this->getTemplatePrefix().$this->getName().'_new',
            'title' => 'new.'.$this->getName(),
            'index_route' => $this->getTemplatePrefix().$this->getName().'_index',
        ));
    }
    
    /**
     * Displays a form to edit an existing  entity.
     *
     * @Route("/edit/{id}")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, $id)
    {
        $repository = $this->getDoctrine()->getRepository($this->getType());    
        $entity = $repository->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity.');
        }
        $editForm = $this->createForm($this->getFormType(),$entity);
        $editForm->handleRequest($request);
        
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->saveEntity($entity);
            // return valid response

            $this->get('session')->getFlashBag()->add(
                'success','entity.save.success'
            );
            if($request->isXmlHttpRequest()){
                $message = 'OK';
                $response = new Response();
                $response->headers->set('Content-Type', 'application/html');
                $response->setContent($message);
                return $response;
            } else {
                return $this->redirectToRoute($this->getTemplatePrefix().$this->getName().'_index');
            }
        }
        // if the form was submitted and the request is AJAX
        if ($editForm->isSubmitted() && $request->isXmlHttpRequest()){
            return $this->render($this->getFormTemplate(), array(
                        'entity' => $entity,
                        'edit_form' => $editForm->createView(),
                    ));
        }
        $deleteForm = $this->createDeleteForm($entity);
        
        return $this->render($this->getEditTemplate(), array(
            'entity' => $entity,
            'form_template' => $this->getFormTemplate(),
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'edit_route' => $this->getTemplatePrefix().$this->getName().'_edit',
            'index_route' => $this->getTemplatePrefix().$this->getName().'_index'
        ));
   }
    
   
    
    
    /**
     * Deletes a entity.
     *
     * @Route("/delete/{id}")
     * @Method({"DELETE", "POST"})
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository($this->getType());    
        $entity = $repository->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity.');
        }
        $form = $this->createDeleteForm($entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            try {
                $entityManager->remove($entity);
                $entityManager->flush();
            } catch(\Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException  $fke){
                $this->get('session')->getFlashBag()->add(
                    'success','entity.delete.fkerror'
                );
                return $this->editAction($request, $entity->getId());
            } catch(\Exception  $e){
                $this->get('session')->getFlashBag()->add(
                    'success','entity.delete.error'
                );
                return $this->editAction($request, $entity->getId());
            }
            $this->get('session')->getFlashBag()->add(
                'success','entity.delete.success'
            );
        }

       return $this->redirectToRoute($this->getTemplatePrefix().$this->getName().'_index');
    }

    /**
     * Creates a form to delete a Post entity by id.
     *
     * This is necessary because browsers don't support HTTP methods different
     * from GET and POST. Since the controller that removes the blog posts expects
     * a DELETE method, the trick is to create a simple form that *fakes* the
     * HTTP DELETE method.
     * See http://symfony.com/doc/current/cookbook/routing/method_parameters.html.
     *
     * @param Post $post The post object
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createDeleteForm($entity)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($this->getTemplatePrefix().$this->getName().'_delete', ['id' => $entity->getId()]))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
    protected function saveEntity($entity){
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
    }
    
    protected function createEntity(){
        $type = $this->getType();
        $entity = new $type();

        return $entity;
    }
    
    
    /**
     * Make join on tables depending on the sorted column
     * By default it's empty
     * 
     * @param QueryBuilder $query
     * @param string $column
     */
    protected function addJoinBySortColumn(QueryBuilder $queryBuilder, $column){
        // Make join on tables depending on the sorted column
         switch ($column){
			 /*
            case "status.orderNumber":
                \AppBundle\Utils\DoctrineHelper::joinIfExists($queryBuilder, 'user.status', 'status');
                break;*/
            default:
                break;
        }
    }
}
