<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/")
 * Class DefaultController
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(){
        if ($this->getUser()){
            $users = $this->getDoctrine()->getRepository(User::class)->findAll();
        } else {
            $users = [];
        }
        return $this->render('default/index.html.twig', ['users' => $users]);
    }

}
