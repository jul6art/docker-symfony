<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null, ['label' => 'user.username'])
            ->add('email', EmailType::class, ['label' => 'user.email'])
            ->add('enabled', CheckboxType::class, ['label' => 'user.enabled', 'required' => false])
            ->add('locked', CheckboxType::class, ['label' => 'user.locked', 'required' => false])
			->add('lastname',null, ['label' => 'user.lastname'])
			->add('firstname',null, ['label' => 'user.firstname'])
			->add('street',null, ['label' => 'user.street'])
			->add('city',null, ['label' => 'user.city'])
			->add('zipcode',null, ['label' => 'user.zipcode'])
			->add('phone',null, ['label' => 'user.phone']);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }


}
